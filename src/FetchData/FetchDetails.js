import axios from "axios";
import { useQuery } from "react-query";
import { fetchIBMKey } from "../Component/queryKeys";
import { FunctionName, ApiKeys } from "./ApiKeys";
import { BehaviorSubject } from "rxjs";
import React, { createContext, useContext } from "react";
import moment from "moment";

export const fetchPosts = async (symbol) => {
  const res = await axios.get(
    `https://www.alphavantage.co/query?function=${FunctionName}&symbol=${symbol}&interval=5min&apikey=${ApiKeys}`
  );
  return res?.data;
};

export const ibmStockData$ = new BehaviorSubject();

export const FetchData = (symbol) => {
  const response = useQuery([fetchIBMKey, symbol], () => fetchPosts(symbol));
  response.expand = false;
  ibmStockData$.next(response);
};

export const stockTickerData$ = new BehaviorSubject();

export function SocketData() {
  const socket = new WebSocket("wss://finnhub.io/ws");
  socket.addEventListener("open", function (event) {
    socket.send(JSON.stringify({ type: 50, ticker: "BINANCE:BTCUSDT" }));
    socket.send(JSON.stringify({ type: 50, ticker: "BINANCE:ETHUSDT" }));
    socket.send(JSON.stringify({ type: 50, ticker: "OANDA:EUR_USD" }));
    socket.send(JSON.stringify({ type: 50, ticker: "OANDA:GBP_USD" }));
    socket.send(JSON.stringify({ type: 50, ticker: "AAPL.US" }));
    socket.send(JSON.stringify({ type: 50, ticker: "MSFT.US" }));
    socket.send(JSON.stringify({ type: 50, ticker: "AMZN.US" }));
  });

  socket.addEventListener("message", function (event) {
    const response = JSON.parse(event.data);
    calculate(response.content);
  });
}
let tickerValue = {};
const calculate = (content) => {
  let obj = {
    displayName: "",
    prevClose: 0.0,
    price: 0.0,
    diff: 0.0,
    percentage: 0.0,
  };
  //  hasOwnProperties customprototype
  if (content && Object.keys(content).indexOf("prevClose") >= 0) {
    obj.prevClose = content.prevClose;
    obj.displayName = content.displayName;
    obj.percentage =
      tickerValue[content.ticker] &&
      tickerValue[content.ticker].percentage !== undefined
        ? tickerValue[content.ticker].percentage
        : obj.percentage;
    obj.diff =
      tickerValue[content.ticker] &&
      tickerValue[content.ticker].price !== undefined
        ? parseFloat(tickerValue[content.ticker].price - content.prevClose)
        : 0.0;
    obj.price =
      tickerValue[content.ticker] &&
      tickerValue[content.ticker].price !== undefined
        ? tickerValue[content.ticker].price
        : obj.price;
  } else {
    obj.prevClose =
      tickerValue[content.ticker] &&
      tickerValue[content.ticker].prevClose !== undefined
        ? tickerValue[content.ticker].prevClose
        : obj.prevClose;
    obj.displayName =
      tickerValue[content.ticker] &&
      tickerValue[content.ticker].displayName !== undefined
        ? tickerValue[content.ticker].displayName
        : obj.displayName;
    obj.price = content.price;
    const diff =
      tickerValue[content.ticker] &&
      tickerValue[content.ticker].prevClose !== undefined
        ? parseFloat(content.price - tickerValue[content.ticker].prevClose)
        : 0.0;
    const percentage = parseFloat((diff / content.price) * 100);
    obj.diff = diff.toFixed(2);
    obj.percentage = percentage.toFixed(2);
  }
  let updatedValue = tickerValue;
  updatedValue[content.ticker] = obj;
  tickerValue = { ...tickerValue, ...updatedValue };
  stockTickerData$.next(tickerValue);
};

export const stockTradeList$ = new BehaviorSubject();
const key = "c86e23iad3i9fvji8icg";

export function SocketTradeData() {
  const socket = new WebSocket(`wss://ws.finnhub.io?token=${key}`);
  socket.addEventListener("open", function (event) {
    socket.send(
      JSON.stringify({ type: "subscribe", symbol: "BINANCE:BTCUSDT" })
    );
    socket.send(
      JSON.stringify({ type: "subscribe", symbol: "BINANCE:ETHUSDT" })
    );
    socket.send(JSON.stringify({ type: "subscribe", symbol: "OANDA:EUR_USD" }));
    socket.send(JSON.stringify({ type: "subscribe", symbol: "OANDA:GBP_USD" }));
    socket.send(JSON.stringify({ type: "subscribe", symbol: "AAPL.US" }));
    socket.send(JSON.stringify({ type: "subscribe", symbol: "MSFT.US" }));
    socket.send(JSON.stringify({ type: "subscribe", symbol: "AMZN.US" }));
  });

  socket.addEventListener("message", function (event) {
    const response = JSON.parse(event.data);
    calculateTradeData(response.data);
  });
}

let socketTradeData = {};
const calculateTradeData = (data) => {
  let updatedValue = socketTradeData;
  data &&
    data.forEach((value) => {
      let obj = {
        time: moment(value.t).format("YYYY-MM-DD HH:MM:SS"),
        price: value.p,
        volume: value.v,
      };
      if (updatedValue.hasOwnProperty(value.s)) {
        updatedValue[value.s].unshift(obj);
      } else {
        updatedValue[value.s] = [obj];
      }
    });
  socketTradeData = { ...updatedValue };
  stockTradeList$.next(socketTradeData);
};
const StockDataContext = createContext({
  ibmStockData$,
  stockTickerData$,
  stockTradeList$,
});

export const useStockData = () => useContext(StockDataContext);

export const StockDataProvider = ({ children }) => (
  <StockDataContext.Provider
    value={{
      ibmStockData$,
      stockTickerData$,
      stockTradeList$,
    }}
  >
    {children}
  </StockDataContext.Provider>
);
