import { ibmStockData$ } from "../FetchData/FetchDetails";
import React, { useState } from "react";
import { useObservableState } from "observable-hooks";
import {
  Card,
  IconButton,
  Collapse,
  CardContent,
  CardHeader,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import { styled } from "@mui/material/styles";

export const IBMStockData = () => {
  const [expanded, setExpanded] = useState(false);
  const data = useObservableState(ibmStockData$);
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
  })(({ theme, expand }) => ({
    transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  }));
  const keys = "Time Series (5min)";
  return (
    <div>
      {data.isLoading ? (
        <div>Loading...</div>
      ) : data.isError ? (
        <div>An error while fetching posts</div>
      ) : (
        <Card>
          <CardHeader
            title="Stock Data IBM"
            action={
              <ExpandMore
                expand={expanded}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
              >
                <ExpandMoreIcon />
              </ExpandMore>
            }
          />
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <CardContent>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Time</TableCell>
                    <TableCell align="centre">Open</TableCell>
                    <TableCell align="centre">High</TableCell>
                    <TableCell align="centre">Low</TableCell>
                    <TableCell align="centre">Close</TableCell>
                    <TableCell align="centre">Volume</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data &&
                    data.data &&
                    Object.keys(data.data[keys]).map((key) => (
                      <TableRow>
                        <TableCell>{key}</TableCell>
                        {Object.keys(data.data[keys][key]).map((ele) => (
                          <TableCell>{data.data[keys][key][ele]}</TableCell>
                        ))}
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </CardContent>
          </Collapse>
        </Card>
      )}
    </div>
  );
};
