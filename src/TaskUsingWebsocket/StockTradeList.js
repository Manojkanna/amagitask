import { useObservableState } from "observable-hooks";
import { stockTradeList$ } from "../FetchData/FetchDetails";
import React from "react";
import {
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  TableContainer,
  Paper,
} from "@mui/material";
import "./style.css";

export const StockTradeList = () => {
  const data = useObservableState(stockTradeList$);

  return (
    <div>
      {data &&
        Object.keys(data).map((key) => (
          <div className="Table">
            {key}
            <Paper sx={{ width: "100%", overflow: "hidden" }}>
              <TableContainer sx={{ maxHeight: "500px" }}>
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell>Time</TableCell>
                      <TableCell>Price</TableCell>
                      <TableCell>Volume</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data[key].map((value) => (
                      <TableRow>
                        <TableCell>{value.time} IST</TableCell>
                        <TableCell>{value.price}</TableCell>
                        <TableCell>{value.volume}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
          </div>
        ))}
    </div>
  );
};
