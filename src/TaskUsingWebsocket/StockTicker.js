import React from "react";
import { Card, CardContent, CardHeader } from "@mui/material";
import { useObservableState } from "observable-hooks";
import { stockTickerData$ } from "../FetchData/FetchDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";

export const StockTicker = () => {
  const tickerValue = useObservableState(stockTickerData$);

  return (
    <div
      style={{
        marginTop: "20px",
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
      }}
    >
      {tickerValue &&
        Object.values(tickerValue).map((ticker) => (
          <div style={{ width: "250px", padding: "10px" }}>
            <Card style={{ width: "100%" }}>
              <CardHeader
                title={ticker.displayName}
                action={
                  <span style={{ marginTop: "10px" }}>{ticker.prevClose}</span>
                }
              />
              <CardContent>
                <div style={{ display: "flex", flexDirection: "row" }}>
                  <div style={{ flex: 1 }}>
                    <span>
                      {ticker.percentage < 0 ? (
                        <ExpandMoreIcon style={{ color: "red" }} />
                      ) : (
                        <ExpandLessIcon style={{ color: "green" }} />
                      )}
                    </span>
                    <span
                      style={{
                        fontSize: "20px",
                        color: ticker.percentage < 0 ? "red" : "green",
                      }}
                    >
                      {ticker.percentage}%
                    </span>
                  </div>
                  <div
                    style={{
                      flex: 1,
                      paddingTop: "5px",
                      color: ticker.percentage < 0 ? "red" : "green",
                    }}
                  >
                    ({ticker.diff})
                  </div>
                </div>
              </CardContent>
            </Card>
          </div>
        ))}
    </div>
  );
};

// export default StockTicker;
