import { StockDataProvider } from "../FetchData/FetchDetails";
import { SocketData, SocketTradeData } from "../FetchData/FetchDetails";
import { StockTicker } from "../TaskUsingWebsocket/StockTicker";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import { Button } from "@mui/material";
import { StockTradeList } from "../TaskUsingWebsocket/StockTradeList";

export function Home() {
  const socketTicket = () => {
    SocketData();
  };
  const socketTradeList = () => {
    SocketTradeData();
  };
  return (
    <StockDataProvider>
      <div>
        <Router>
          <div>
            <nav>
              <Button variant="outlined" onClick={() => socketTradeList()}>
                <Link to="/stockTradeList">Stock Trade List</Link>
              </Button>

              <Button variant="outlined" onClick={() => socketTicket()}>
                <Link to="/">Stock Ticker</Link>
              </Button>
            </nav>
          </div>
          <div>
            <Routes>
              <Route path="/stockTradeList" element={<StockTradeList />} />
              <Route path="/" element={<StockTicker />} />
            </Routes>
          </div>
        </Router>
      </div>
    </StockDataProvider>
  );
}
